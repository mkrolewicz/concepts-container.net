﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace PK.Container
{
    class Container : IContainer
    {
        private static readonly IDictionary<Type, Type> types = new Dictionary<Type, Type>();
        private static readonly Dictionary<Type, object> rot = new Dictionary<Type, object>();
        private static readonly object[] emptyArguments = new object[0];

        public void Register(System.Reflection.Assembly assembly)
        {
            Type[] localTypes;
            localTypes = assembly.GetTypes();
            foreach (Type singleType in localTypes)
            {
                this.Register(singleType);
            }
        }

        public void Register(Type type)
        {
            Type[] localTypes = type.GetInterfaces();

            foreach (Type singleType in localTypes)
            {
                if (types.ContainsKey(singleType))
                {
                    types[singleType] = type;
                }
                else
                {
                    types.Add(type, singleType);
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            types.Add(typeof(T), null);
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            this.Register(typeof(T));
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (!types.ContainsKey(type))
            {
                throw new PK.Container.UnresolvedDependenciesException();
            }

            Type resolvedType = types[type];

            ConstructorInfo constructor = resolvedType.GetConstructors().First();
            ParameterInfo[] param = constructor.GetParameters();

            if (!param.Any())
            {
                return Activator.CreateInstance(resolvedType);
            }
            else
            {
                return constructor.Invoke(ResolveParameters(param).ToArray();
            }
        }

        private IEnumerable<object> ResolveParameters(IEnumerable<ParameterInfo> param)
        {
            return param.Select(p => Resolve(p.ParameterType)).ToList();
        }

       

    }
}
